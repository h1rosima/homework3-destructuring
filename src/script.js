//1
const clients1 = [
    'Гилберт',
    'Сальваторе',
    'Пирс',
    'Соммерс',
    'Форбс',
    'Донован',
    'Беннет',
];
const clients2 = ['Пирс', 'Зальцман', 'Сальваторе', 'Майклсон'];

const allClients = [...clients1, ...clients2];
console.log(`1-е задание - ${allClients}`);

//2
const characters = [
    {
        name: 'Елена',
        lastName: 'Гилберт',
        age: 17,
        gender: 'woman',
        status: 'human',
    },
    {
        name: 'Кэролайн',
        lastName: 'Форбс',
        age: 17,
        gender: 'woman',
        status: 'human',
    },
    {
        name: 'Аларик',
        lastName: 'Зальцман',
        age: 31,
        gender: 'man',
        status: 'human',
    },
    {
        name: 'Дэймон',
        lastName: 'Сальваторе',
        age: 156,
        gender: 'man',
        status: 'vampire',
    },
    {
        name: 'Ребекка',
        lastName: 'Майклсон',
        age: 1089,
        gender: 'woman',
        status: 'vampire',
    },
    {
        name: 'Клаус',
        lastName: 'Майклсон',
        age: 1093,
        gender: 'man',
        status: 'vampire',
    },
];
const charactersShortInfo = characters.map(({ name, lastName, age }) => ({
    name,
    lastName,
    age,
}));

console.log(`2-е задание - ${JSON.stringify(charactersShortInfo)}`);

//3
const user1 = {
    name: 'John',
    years: 30,
    //isAdmin ?
};

const { name: Name = 'Имя', years: Age = 'Возраст', isAdmin = 'false' } = user1;
console.log(`3-е задание - ${Name}, ${Age}, ${isAdmin} `);

//4
const satoshi2020 = {
    name: 'Nick',
    surname: 'Sabo',
    age: 51,
    country: 'Japan',
    birth: '1979-08-21',
    location: {
        lat: 38.869422,
        lng: 139.876632,
    },
};

const satoshi2019 = {
    name: 'Dorian',
    surname: 'Nakamoto',
    age: 44,
    hidden: true,
    country: 'USA',
    wallet: '1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa',
    browser: 'Chrome',
};

const satoshi2018 = {
    name: 'Satoshi',
    surname: 'Nakamoto',
    technology: 'Bitcoin',
    country: 'Japan',
    browser: 'Tor',
    birth: '1975-04-05',
};

const satoshiInfo = { ...satoshi2018, ...satoshi2019, ...satoshi2020 };
console.log(`4-e задание - ${JSON.stringify(satoshiInfo)}`);
// console.log(satoshiInfo);       // без JSON преобразования

//5
const books = [
    {
        name: 'Harry Potter',
        author: 'J.K. Rowling',
    },
    {
        name: 'Lord of the rings',
        author: 'J.R.R. Tolkien',
    },
    {
        name: 'The witcher',
        author: 'Andrzej Sapkowski',
    },
];

const bookToAdd = {
    name: 'Game of thrones',
    author: 'George R. R. Martin',
};

const bookshelf = [...books, bookToAdd];
console.log(`5-e задание - ${JSON.stringify(bookshelf)}`);

// console.log('5-e задание');          // без JSON преобразования
// console.log(bookshelf);

//6
const employee = {
    name: 'Vitalii',
    surname: 'Klichko',
};

const employeeSubInfo = {
    age: 52,
    salary: 1003636,
};

const employeeInfo = { ...employee, ...employeeSubInfo };
console.log(
    `6-е задание - Согласно декларации, в 2023 году весь доход Кличко составляла его заработная плата из Киевской городской государственной администрации – ${employeeInfo.salary} грн.`
);

//7
const array = ['value', () => 'showValue'];
let value = array[0];
let showValue = array[1];

alert(value); // має бути виведено 'value'
alert(showValue()); // має бути виведено 'showValue'
